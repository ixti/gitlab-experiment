# frozen_string_literal: true

# Add in a reset! method that can be used in tests to reset an experiment back
# to a known state with the context -- basically an unresolved variant state,
# with a given context.
class Gitlab::Experiment
  # add in a reset method
  def reset!(context = nil)
    @variant_name = nil
    context(context) if context
  end
end

# Define our base ApplicationExperiment, which we used to simulate when
# configuration specifies a different (and more like a Rails) value.
class ApplicationExperiment < Gitlab::Experiment
end

# We use this our testing of RSpec helpers and custom matchers, and may change
# this class in specs -- don't use it outside of the rspec_spec.rb file.
class StubExperiment < Gitlab::Experiment
end

# An experiment that permits nested experiments. It's used to test that our
# nested experiment tracking module works and builds the correct structures.
class NestableExperiment < Gitlab::Experiment
  include Gitlab::Experiment::TestBehaviors::Trackable

  def control_behavior; end

  def nest_experiment(_); end
end
