# frozen_string_literal: true

require_relative 'environment'

# Run the Rails application.
run Rails.application
