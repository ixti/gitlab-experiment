# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Cache::RedisHashStore do
  subject { described_class.new(pool: ->(&block) { block.call(redis) }) }

  let(:key_name) { 'experiment' }
  let(:field_name) { 'abc123' }
  let(:key_field) { [key_name, field_name].join(':') }
  let(:redis) { Redis.current }

  around do |example|
    redis.del(key_name)
    example.run
    redis.del(key_name)
  end

  it "allows reading, writing and deleting", :aggregate_failures do
    # test them all together because they are largely interdependent

    expect(subject.read(key_field)).to be_nil
    expect(redis.hget(key_name, field_name)).to be_nil

    subject.write(key_field, 'value')

    expect(subject.read(key_field)).to eq('value')
    expect(redis.hget(key_name, field_name)).to eq('value')

    subject.delete(key_field)

    expect(subject.read(key_field)).to be_nil
    expect(redis.hget(key_name, field_name)).to be_nil
  end

  it "handles the fetch with a block behavior (which is read/write)" do
    expect(subject.fetch(key_field) { 'value1' }).to eq('value1') # rubocop:disable Style/RedundantFetchBlock
    expect(subject.fetch(key_field) { 'value2' }).to eq('value1') # rubocop:disable Style/RedundantFetchBlock
  end

  it "can increment a value in a hash" do
    expect(subject.increment(key_field)).to eq(1)
    expect(subject.increment(key_field)).to eq(2)
    expect(subject.increment(key_field, -3)).to eq(-1)
  end

  it "can clear a whole experiment cache key" do
    subject.write(key_field, 'value')
    subject.write("#{key_field}_attrs", 'value')

    subject.clear(key: key_field)

    expect(redis.get(key_name)).to be_nil
    subject.write("#{key_field}_attrs", 'value')
  end

  it "doesn't allow clearing a key from the cache that's not a hash (definitely not an experiment)" do
    redis.set(key_name, 'value')

    expect { subject.clear(key: key_name) }.to raise_error(
      ArgumentError,
      'invalid call to clear a non-hash cache key'
    )
  end
end
