# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::TestBehaviors::Trackable, :experiment do
  let(:tracked) { Gitlab::Experiment::TestBehaviors::TrackedStructure }

  before do
    allow(config).to receive(:base_class).and_return('NestableExperiment')
    allow(config).to receive(:name_prefix).and_return(nil)
  end

  describe "a nested and tracked scenario" do
    let(:ex1)     { experiment(:ex1)     { |e| e.use { ex1_1 && ex1_2 } } }
    let(:ex1_1)   { experiment(:ex1_1)   { |e| e.use { ex1_1_1 && experiment(:ex1_1_1) {} } } }
    let(:ex1_1_1) { experiment(:ex1_1_1) {} }
    let(:ex1_2)   { experiment(:ex1_2)   {} }
    let!(:ex2)    { experiment(:ex2)     {} }

    it "maintains a nesting structure that can be evaluated holistically" do
      ex1.run
      expect(tracked.hierarchy.deep_stringify_keys).to eq(JSON.parse(<<~JSON))
        {
          "ex2": {"name": "ex2", "count": 1, "children": {}},
          "ex1": {
            "name": "ex1",
            "count": 1,
            "children": {
              "ex1_1": {
                "name": "ex1_1",
                "count": 1,
                "children": {
                  "ex1_1_1": {"name": "ex1_1_1", "count": 2, "children": {}},
                  "ex1_2": {"name": "ex1_2", "count": 1, "children": {}
                  }
                }
              }
            }
          }
        }
      JSON
    end

    it "tracks a list of dependencies for all experiments that have been run" do
      ex1.run
      expect(tracked.dependencies.deep_stringify_keys).to eq(JSON.parse(<<~JSON))
        {
          "ex2": [],
          "ex1": [],
          "ex1_1": ["ex1"],
          "ex1_1_1": ["ex1", "ex1_1"],
          "ex1_2": ["ex1"]
        }
      JSON
    end
  end
end
