# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Nestable do
  let(:stack) { described_class::Stack }
  let(:nested_experiment) { experiment(:level2) { |e| e.use {} } }

  it "raises an exception on nested experiments" do
    expect { experiment(:level1) { |e| e.use { nested_experiment } } }.to raise_error(
      Gitlab::Experiment::NestingError,
      "unable to nest the gitlab_experiment_level2 experiment within the gitlab_experiment_level1 experiment"
    )
  end

  it "keeps track of the stack" do
    step = 0
    begin
      experiment(:level1) do |e|
        e.use do
          step += 1
          expect(stack.size).to eq(1)
          expect(stack[0]).to eq(e) # it's us!

          raise Gitlab::Experiment::Error, "let's raise an exception!"
        end
      end
    rescue Gitlab::Experiment::Error
      step += 1
      expect(stack.size).to eq(0)
    end

    expect(step).to eq(2)
  end

  describe "when nesting is allowed" do
    before do
      allow(config).to receive(:base_class).and_return('NestableExperiment')
    end

    it "doesn't raise an exception" do
      expect { experiment(:level1) { |e| e.use { nested_experiment } } }.not_to raise_error
    end

    it "keeps track of the stack" do
      experiment(:level1) do |e1|
        e1.use do
          expect(stack.size).to eq(1)
          expect(stack[0]).to eq(e1) # it's us!
          experiment(:level2) do |e2|
            e2.use do
              expect(stack.size).to eq(2)
              expect(stack[1]).to eq(e2)
            end
          end
        end
      end
    end
  end
end
