# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment do
  subject { described_class.new(:example, **context, &variants) }

  let(:experience) { double(control: '_control_', candidate: '_candidate_', variant: '_variant_') }
  let(:context) { { actor: 42 } }
  let(:expected_name) { 'gitlab_experiment_example' }
  let(:variants) do
    lambda do |e|
      e.use { experience.control }
      e.try { experience.candidate }
      e.try(:variant) { experience.variant }
    end
  end

  it "allows for clean configuration" do
    expect(config).to receive(:name_prefix=).with('_name_prefix_')

    described_class.configure { |c| c.name_prefix = '_name_prefix_' }
  end

  context "when using the experiment dsl" do
    subject { nil } # don't use subject in here

    it "runs the control" do
      expect(experiment(:example, &variants)).to be_a(described_class)

      expect(experience).to have_received(:control)
    end

    it "runs a variant if instructed to" do
      experiment(:example, 'candidate', &variants)

      expect(experience).to have_received(:candidate)
    end

    it "falls back to control if the variant resolver returns blank" do
      experiment(:example, &variants)

      expect(experience).to have_received(:control)
    end

    it "can run a method if a behavior hasn't been defined" do
      called = false
      described_class.define_method(:alt_variant_behavior) { called = true }

      experiment(:example, :alt_variant, &variants)

      expect(called).to be_truthy
    ensure
      described_class.undef_method(:alt_variant_behavior)
    end

    it "prioritizes block behaviors over method behaviors" do
      called = false
      described_class.define_method(:candidate_behavior) { called = true }

      experiment(:example, :candidate, &variants)

      expect(called).to be_falsey
      expect(experience).to have_received(:candidate)
    ensure
      described_class.undef_method(:candidate_behavior)
    end

    it "returns the result of the experiment when run is called within the provided block" do
      result = experiment(:example) { |e| variants.call(e) && e.run }

      expect(result).to eq('_control_')
    end

    it "doesn't track more than once when run is called within the provided block" do
      expect(config).to receive(:tracking_behavior).once.and_return(->(*) {})

      experiment(:example) do |e|
        e.use { nil }
        e.try { nil }
        e.run
      end
    end

    it "has a valid signature" do
      subject = experiment(:example, :candidate, **context, &variants)

      expect(subject.signature).to eq(variant: 'candidate', experiment: expected_name, key: key_for(subject))
    end

    it "handles records in the context", :rails do
      subject = experiment(:example, record: double(to_global_id: double(to_s: 'gid://Dummy/Record/42')))

      expect(subject.signature[:key]).to eq('606d81ccb5bd8ab0d624da42ae77c1a08054443124cb5f8d2dde9486fd80c935')
    end

    it "has id-able methods for urls, and flipper / unleash" do
      subject = experiment(:example, **context, &variants)

      expect(subject.id).to eq("#{expected_name}:#{key_for(subject)}")
      expect(subject.to_param).to eq(subject.id)
      expect(subject.session_id).to eq(subject.id)
      expect(subject.flipper_id).to eq("Experiment;#{subject.id}")
    end

    it "doesn't have stack issues when using signature to determine variant" do
      result = nil
      allow(config).to receive(:inclusion_resolver).and_return(->(*) { result = signature })

      experiment(:example, &variants)

      expect(result).to include(experiment: expected_name, variant: 'unresolved')
    end

    it "allows getting all variant names for an experiment" do
      result = experiment(:example, &variants).variant_names

      expect(result).to eq(%i[candidate variant])
    end

    it "only runs the control when the experiment isn't enabled" do
      allow_any_instance_of(described_class).to receive(:enabled?).and_return(false)
      expect(config).not_to receive(:inclusion_resolver)

      subject = experiment(:example, &variants)

      expect(subject.variant.name).to eq('control')
      expect(experience).to have_received(:control)
    end

    it "uses caching if configured to do so" do
      cache = double(fetch: 'candidate')
      allow(config).to receive(:cache).and_return(cache)

      subject = experiment(:example, &variants)

      expect(experience).to have_received(:candidate)
      expect(cache).to have_received(:fetch).with("#{expected_name}:#{key_for(subject)}")
    end

    it "caches the variant on run" do
      cache = double(fetch: nil, write: nil)
      allow(config).to receive(:cache).and_return(cache)

      subject = experiment(:example, :candidate, &variants)

      expect(experience).to have_received(:candidate)
      expect(cache).to have_received(:write).with("#{expected_name}:#{key_for(subject)}", :candidate)
    end

    it "can track from within the block" do
      signature = %(:variant=>"control", :experiment=>"#{expected_name}", :key=>"#{key_for(expected_name, context)}")
      expect(config.logger).to receive(:info).with <<~LOG.strip
        Gitlab::Experiment[#{expected_name}] _event_: {:signature=>{#{signature}}}
      LOG

      experiment(:example, **context) do |e|
        e.use {}
        e.track('_event_')
      end
    end

    describe 'when using the control/candidate dsl' do
      let(:variants) do
        lambda do |e|
          e.control {}
          e.candidate(:red) {}
          e.candidate(:blue) {}
        end
      end

      it 'allows using control and candidate methods' do
        expect(experiment(:example, &variants).behaviors.keys).to eq(%w[control red blue])
      end
    end
  end

  context "when the control should be used" do
    it "runs the control" do
      expect(subject.run).to eq('_control_')

      expect(experience).to have_received(:control)
      expect(experience).not_to have_received(:candidate)

      # it was memoized
      expect(subject.instance_variable_get(:@variant_name)).to eq(:control)
    end

    it "runs a variant if instructed to" do
      subject.variant(:candidate)

      expect(subject.run).to eq('_candidate_')

      expect(experience).not_to have_received(:control)
      expect(experience).to have_received(:candidate)
    end

    it "allows specifying the variant to #run" do
      subject.variant(:candidate)

      expect(subject.run(:variant)).to eq('_variant_')

      expect(experience).not_to have_received(:control)
      expect(experience).not_to have_received(:candidate)
      expect(experience).to have_received(:variant)
    end

    it "raises an exception if the variant is unknown" do
      subject.variant(:foo)

      expect { subject.run }.to raise_error(described_class::Error, "#{expected_name} missing foo behavior")
    end

    it "publishes the results" do
      allow(subject).to receive(:instance_exec).and_call_original

      subject.run

      expect(subject).to have_received(:instance_exec).with(
        instance_of(Scientist::Result),
        &config.publishing_behavior
      )

      expect(described_class.published_experiments).to eq(
        'gitlab_experiment_example' => {
          excluded: false,
          experiment: 'gitlab_experiment_example',
          key: '965da49caebde3a7c9b4730c214081fcc47dd451f085a47489a22c6afb6ad655',
          variant: 'control'
        }
      )
    end
  end

  context "when a variant should be used" do
    before do
      stub_experiments(example: true)
    end

    it "runs the variant" do
      subject.run

      expect(experience).not_to have_received(:control)
      expect(experience).to have_received(:candidate)

      # it was memoized
      expect(subject.instance_variable_get(:@variant_name)).to eq(:candidate)
    end

    it "runs the control if instructed to" do
      subject.variant(:control)

      subject.run

      expect(experience).to have_received(:control)
      expect(experience).not_to have_received(:candidate)
    end

    it "raises an exception if the variant is unknown" do
      subject.variant(:foo)

      expect { subject.run }.to raise_error(described_class::Error, "#{expected_name} missing foo behavior")
    end
  end

  context "when utilizing caching" do
    before do
      allow(subject).to receive(:run_callbacks).and_yield
    end

    it "doesn't do runtime segmentation if a cached (non blank) value is present" do
      expect(subject).to receive(:cache_variant).and_return('control')

      subject.run

      expect(subject).to have_received(:run_callbacks).with(:unsegmented)
    end

    it "executes runtime segmentation if no specific variant was resolved or cached" do
      expect(subject).to receive(:cache_variant).and_return('')

      subject.run

      expect(subject).to have_received(:run_callbacks).with(:exclusion_check)
      expect(subject).to have_received(:run_callbacks).with(:segmentation_check)
    end
  end

  context "when specifying rollouts" do
    let(:subject_experiment) { StubExperiment }

    before do
      subject_experiment.instance_variable_set(:'@rollout', nil)
    end

    it "allows setting the default rollout" do
      expect(subject_experiment.default_rollout).to be_an_instance_of(Gitlab::Experiment::Rollout::Base)

      subject_experiment.default_rollout(Gitlab::Experiment::Rollout::Random)

      expect(subject_experiment.default_rollout).to be_an_instance_of(Gitlab::Experiment::Rollout::Random)
      expect(subject_experiment.new.rollout).to eq(subject_experiment.default_rollout)

      # it's not changed the base class
      expect(described_class.default_rollout).to be_an_instance_of(Gitlab::Experiment::Rollout::Base)
    end

    it "allows setting the rollout by simple name" do
      subject_experiment.default_rollout(:round_robin)
      subject = subject_experiment.new

      expect(subject.rollout).to be_an_instance_of(Gitlab::Experiment::Rollout::RoundRobin)

      subject.rollout(:random)

      expect(subject.rollout).to be_an_instance_of(Gitlab::Experiment::Rollout::Random)
    end
  end

  context "when tracking events" do
    let(:context) { { actor: 42, model: double(to_global_id: 'gid://dummy-app/Model/1') } }

    it "consistently associates a given event with the correct signature" do
      prefix = %(Gitlab::Experiment[#{expected_name}])
      signature = %(:variant=>"control", :experiment=>"#{expected_name}", :key=>"#{key_for(subject)}")
      expect(config.logger).to receive(:info).with <<~LOG.strip
        #{prefix} action: {:label=>"_label_", :signature=>{#{signature}}}
      LOG

      subject.track(:action, label: '_label_')
    end

    it "doesn't track if the context doesn't allow it" do
      expect(subject.context).to receive(:trackable?).and_return(false)
      expect(config.logger).not_to receive(:info)

      subject.track(:action)
    end
  end

  context "when specifying sticky_to" do
    it "will remain sticky to the provided value" do
      expected_key = key_for(expected_name, 'a')
      subject.context(version: 0, sticky_to: 'a')

      expect(subject.signature).to eq(key: expected_key, variant: 'control', experiment: expected_name)

      subject.context(version: 1)

      expect(subject.signature).to eq(key: expected_key, variant: 'control', experiment: expected_name)
    end
  end

  context "when migrating context" do
    context "with a partial migration" do
      let(:keys) do
        [
          key_for(expected_name, actor: 42, version: 0),
          key_for(expected_name, actor: 42, version: 1),
          key_for(expected_name, actor: 42, version: 2)
        ]
      end

      it "generates the correct tracking signature" do
        subject.context(version: 0)

        expect(subject.signature).to eq(key: keys[0], variant: 'control', experiment: expected_name)

        subject.context(version: 1, migrated_with: { version: 0 })

        expect(subject.signature).to include(key: keys[1], migration_keys: keys[0..0])

        subject.context(version: 2, migrated_with: { version: 1 })

        expect(subject.signature).to include(key: keys[2], migration_keys: keys[0..1])
      end
    end

    context "with a full migration" do
      let(:context) { {} }

      let(:keys) do
        [
          key_for(expected_name, version: 0),
          key_for(expected_name, version: 1),
          key_for(expected_name, version: 2)
        ]
      end

      it "generates the correct tracking signature" do
        subject.context(version: 0)

        expect(subject.signature).to eq(key: keys[0], variant: 'control', experiment: expected_name)

        subject.context(version: 1, migrated_from: { version: 0 })

        expect(subject.signature).to include(key: keys[1], migration_keys: keys[0..0])

        subject.context(version: 2, migrated_from: { version: 1 })

        expect(subject.signature).to include(key: keys[2], migration_keys: keys[0..1])
      end
    end
  end

  context "when not enabled" do
    before do
      allow(subject).to receive(:enabled?).and_return(false)
    end

    it "specifies the requested variant as control" do
      subject.run

      expect(subject.variant.name).to eq('control')
    end

    it "defers to the requested variant if one has been" do
      subject.run(:variant)

      expect(subject.variant.name).to eq('variant')
    end

    it "doesn't allow tracking events" do
      expect(config).not_to receive(:tracking_behavior)

      subject.track(:foo)
    end

    it "doesn't execute segmenting in the run" do
      allow(subject).to receive(:run_callbacks).and_yield

      subject.run

      expect(subject).not_to have_received(:run_callbacks).with(:segmentation_check)
    end

    it "doesn't call the publishing behavior" do
      expect(config).not_to receive(:publishing_behavior)

      subject.run
    end
  end

  context "with exclusion rules" do
    before do
      @exclusions = callbacks = double(first_callback: true, second_callback: nil)
      subject.class.exclude { callbacks.first_callback }
      subject.class.exclude { callbacks.second_callback }
    end

    after do
      subject.class.reset_callbacks(:exclusion_check)
    end

    it "is not considered excluded by default" do
      subject.class.reset_callbacks(:exclusion_check)

      expect(subject.send(:excluded?)).to be_falsey
    end

    it "can use callbacks to determine exclusion" do
      expect(subject.send(:excluded?)).to be_truthy
    end

    it "doesn't continue excluding if we've already determined exclusion" do
      expect(subject.run).to eq('_control_')

      expect(@exclusions).to have_received(:first_callback).once
      expect(@exclusions).not_to have_received(:second_callback)
    end

    it "doesn't do invoke callbacks if we've already determined exclusion" do
      expect(subject.run).to eq('_control_')

      expect(subject.run_callbacks(:exclusion_check) { :not_called }).to be_falsey
      expect(@exclusions).to have_received(:first_callback).once
    end

    it "defers to the requested variant if one has been" do
      expect(subject.run(:variant)).to eq('_variant_')

      expect(subject.variant.name).to eq('variant')
    end

    it "doesn't allow tracking events" do
      expect(config).not_to receive(:tracking_behavior)

      subject.track(:foo)
    end

    it "still calls the publishing behavior" do
      expect(config).to receive(:publishing_behavior).and_call_original

      subject.run
    end

    it "can exclude from within the block" do
      expect(described_class.new('namespaced/stub', &:exclude!)).to be_excluded
    end
  end

  context "with segmentation rules" do
    before do
      @segmentation = callbacks = double(first_callback: true, second_callback: nil)
      subject.class.segment(variant: :candidate) { callbacks.first_callback }
      subject.class.segment(variant: :candidate) { callbacks.second_callback }
    end

    after do
      subject.class.reset_callbacks(:segmentation_check)
    end

    it "sets the variant if one of the callbacks returns true" do
      expect(subject.run).to eq('_candidate_')

      expect(subject.variant.name).to eq('candidate')
    end

    it "doesn't set the variant if a callback doesn't return truthfully" do
      subject.class.reset_callbacks(:segmentation_check)
      subject.class.segment(variant: :candidate) {}

      expect(subject.run).to eq('_control_')

      expect(subject.variant.name).not_to eq('candidate')
    end

    it "doesn't bother segmenting if we've already determined a variant" do
      expect(subject.run(:control)).to eq('_control_')

      expect(@segmentation).not_to have_received(:first_callback)
      expect(subject.variant.name).to eq('control')
    end

    it "doesn't continue segmenting if we've already determined a variant" do
      expect(subject.run).to eq('_candidate_')

      expect(@segmentation).to have_received(:first_callback).once
      expect(@segmentation).not_to have_received(:second_callback)
      expect(subject.variant.name).to eq('candidate')
    end
  end

  context "when resolving instances and names" do
    context "when utilizing caching" do
      before do
        allow(subject).to receive(:run_callbacks).and_yield
      end

      it "caches the variant when assigning it" do
        expect(subject).to receive(:cache_variant).with(:control).and_return('control')

        # by specifically assigning the control variant here, we're guaranteeing
        # that this context will always get the control variant unless we delete
        # the field from the cache (or clear the entire experiment cache) -- or
        # write code that would specify a different variant.
        subject.run(:control)
      end

      it "DOESN'T cache the control if excluded" do
        allow(subject).to receive(:excluded?).and_return(true)
        expect(subject).not_to receive(:cache_variant).with(:control).and_call_original

        subject.run
      end

      it "doesn't cache the control if not trackable", :suppress_warnings do
        allow(subject.context).to receive(:trackable?).and_return(false)
        expect(subject).not_to receive(:cache_variant).with(:control).and_call_original

        subject.run
      end

      it "doesn't do runtime segmentation if a cached (non blank) value is present" do
        expect(subject).to receive(:cache_variant).and_return('control')

        subject.run

        expect(subject).to have_received(:run_callbacks).with(:unsegmented)
      end

      it "executes runtime segmentation if no specific variant was resolved or cached" do
        expect(subject).to receive(:cache_variant).and_return('')

        subject.run

        expect(subject).to have_received(:run_callbacks).with(:exclusion_check)
        expect(subject).to have_received(:run_callbacks).with(:segmentation_check)
      end
    end

    shared_examples "a nameable experiment" do |base_class_name = 'gitlab/experiment', base: described_class, **options|
      let(:suffix) { options[:suffix] ? '_experiment' : '' }

      it "generates the correct names based on the string provided" do
        expect(described_class.experiment_name('stub', **options)).to eq("stub#{suffix}")
        expect(described_class.experiment_name('stub_experiment', **options)).to eq("stub#{suffix}")
        expect(described_class.experiment_name('namespace/stub', **options)).to eq("namespace/stub#{suffix}")
        expect(described_class.experiment_name('namespace/stub_experiment', **options)).to eq("namespace/stub#{suffix}")

        # using safe_constantize makes this largely ok, but we don't program for every possible mistake
        expect(described_class.experiment_name(true, **options)).to eq("true#{suffix}")

        # special cases
        expect(described_class.experiment_name('experiment', **options)).to eq('experiment')
        expect(described_class.experiment_name('namespace/experiment', **options)).to eq('namespace/experiment')

        # fallbacks
        expect(described_class.experiment_name('', **options)).to eq(base_class_name)
        expect(described_class.experiment_name(nil, **options)).to eq(base_class_name)
        expect(described_class.experiment_name({}, **options)).to eq(base_class_name)
      end

      it "looks for a constant matching the experiment name" do
        expect(experiment(:stub)).to be_a(StubExperiment)
      end

      it "allows the base class to be configured" do
        expect(experiment(:foo)).to be_a(base)
      end

      it "determines the name automatically for custom experiments" do
        expect(StubExperiment.new.name).to eq('gitlab_experiment_stub')
      end

      it "requires name on non-subclassed instances" do
        expect { described_class.new('') }.to raise_error(ArgumentError, 'name is required')
      end

      it "can 'find' an experiment from params" do
        subject = described_class.from_param('namespace%2Fexample:abc123')
        expect(subject).to be_a(base)
        expect(subject.signature).to include(experiment: 'gitlab_experiment_namespace/example', key: 'abc123')
      end

      it "initializes the correct class" do
        expect(described_class.from_param('stub:abc123')).to be_a(StubExperiment)
      end

      it "raises an exception when an experiment can't be instantiated" do
        expect { described_class.from_param('namespace/exampleabc123') }.to raise_error(
          ArgumentError,
          'name is required'
        )
      end
    end

    context "without a configured base_class" do
      describe Gitlab::Experiment do
        it_behaves_like "a nameable experiment", 'gitlab/experiment', suffix: true
        it_behaves_like "a nameable experiment", 'gitlab/experiment', suffix: false
      end
    end

    context "with a configured base_class" do
      before do
        allow(config).to receive(:base_class).and_return('ApplicationExperiment')
      end

      describe Gitlab::Experiment do
        it_behaves_like "a nameable experiment", 'gitlab/experiment', suffix: true, base: ApplicationExperiment
        it_behaves_like "a nameable experiment", 'gitlab/experiment', suffix: false, base: ApplicationExperiment
      end

      describe ApplicationExperiment do
        it_behaves_like "a nameable experiment", 'application_experiment', suffix: true
        it_behaves_like "a nameable experiment", 'application', suffix: false
      end
    end
  end

  context "when processing redirect urls" do
    it "doesn't error if the redirect_url_validator is nil" do
      allow(config).to receive(:redirect_url_validator).and_return(nil)

      expect { subject.process_redirect_url('http://gitlab.com') }.not_to raise_error
    end

    it "tracks an event" do
      expect(config.logger).to receive(:info).with <<~LOG.strip
        Gitlab::Experiment[gitlab_experiment_example] visited: #{{
          url: 'https://gitlab.com',
          signature: { variant: 'control', experiment: 'gitlab_experiment_example', key: key_for(subject) }
        }}
      LOG

      subject.process_redirect_url('https://gitlab.com')
    end

    it "returns the url so mutation can happen within the experiment" do
      url = 'https://gitlab.com'
      expect(subject.process_redirect_url(url)).to eq(url)
    end

    shared_examples "a url validator" do
      let(:valid_urls) do
        [
          'https://about.gitlab.com/',
          'https://gitlab.com/',
          'http://docs.gitlab.com',
          'https://docs.gitlab.com/some/path?foo=bar'
        ]
      end

      let(:invalid_urls) do
        [
          'http://badgitlab.com',
          'https://gitlab.com.nefarious.net',
          'https://unknown.gitlab.com',
          "https://badplace.com\nhttps://gitlab.com",
          'https://gitlabbcom',
          'https://gitlabbcom/'
        ]
      end

      it "returns the url on valid urls" do
        valid_urls.each { |url| expect(subject.process_redirect_url(url)).to eq(url) }
      end

      it "returns nil if the url isn't valid" do
        invalid_urls.each { |url| expect(subject.process_redirect_url(url)).to be_nil }
      end
    end

    context "when using the uri parse behavior" do
      before do
        allow(config).to receive(:redirect_url_validator).and_return(lambda do |url|
          %r{\Ahttps?://((about|docs)\.)?gitlab\.com(/|\z)} =~ url
        end)
      end

      it_behaves_like "a url validator"
    end

    context "when using the regexp behavior" do
      before do
        allow(config).to receive(:redirect_url_validator).and_return(lambda do |url|
          (url = URI.parse(url)) && %w[docs.gitlab.com about.gitlab.com gitlab.com].include?(url.host)
        rescue URI::InvalidURIError
          false
        end)
      end

      it_behaves_like "a url validator"
    end
  end
end
