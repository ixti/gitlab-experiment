# frozen_string_literal: true

# See: https://github.com/marcandre/backports/pull/171
unless Symbol.method_defined?(:end_with?)
  class Symbol
    def end_with?(*suffixes)
      to_s.end_with?(*suffixes)
    end
  end
end
