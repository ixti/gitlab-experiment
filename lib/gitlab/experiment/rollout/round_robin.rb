# frozen_string_literal: true

module Gitlab
  class Experiment
    module Rollout
      class RoundRobin < Base
        KEY_NAME = :last_round_robin_variant

        # Requires a cache to be configured.
        #
        # Keeps track of the number of assignments into the experiment group,
        # and uses this to rotate "round robin" style through the variants
        # that are defined.
        #
        # Relatively performant, but requires a cache, and is dependent on the
        # performance of that cache store.
        def execute
          variant_names[(cache.attr_inc(KEY_NAME) - 1) % variant_names.size]
        end
      end
    end
  end
end
